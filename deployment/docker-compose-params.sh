###> DOCKER-COMPOSE params ###
# Conection to database must have docker_db_container_name:
# - DATABASE_URL=mysql://db_user:db_password@docker_db_container_name:3306/db_name
# Conection to redis must have docker_redis_container_name:
# - MESSENGER_TRANSPORT_DSN=redis://docker_redis_container_name:6379/messages
# Path to docker settings
export EMAIL_DOCKER_PATH=$(pwd)/moi-mail-sender-daemon/docker
# Path to project files
export EMAIL_DOCKER_APP_PATH_HOST=$(pwd)/moi-mail-sender-daemon/
# /var/ww/domain_name
export EMAIL_DOCKER_APP_PATH_CONTAINER=/var/www/moi-mail-sender-daemon/
# Path to nginx ssl files
###< DOCKER-COMPOSE params ###
export EMAIL_DOCKER_HOST_PROJECT_PATH=$(pwd)/moi-mail-sender-daemon