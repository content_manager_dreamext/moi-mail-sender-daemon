<?php

namespace App\Message;

class SendEmailMessage
{
    protected $email;
    protected $subject;
    protected $template;
    protected $data;
    protected $locale;

    public function __construct(
        string $email,
        string $subject,
        string $template,
        array $data,
        string $locale
    ) {
        $this->email = $email;
        $this->subject = $subject;
        $this->template = $template;
        $this->data = $data;
        $this->locale = $locale;
    }

    public function getEmail(): string
    {
        return (string)$this->email;
    }

    public function getSubject(): string
    {
        return (string)$this->subject;
    }

    public function getTemplate(): string
    {
        return (string)$this->template;
    }

    public function getData(): array
    {
        return (array)$this->data;
    }

    public function getLocale(): string
    {
        return (string)$this->locale;
    }
}