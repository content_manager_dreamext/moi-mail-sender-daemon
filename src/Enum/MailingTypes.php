<?php

namespace App\Enum;

class MailingTypes
{
    public const REGISTER_CLIENT = 'registration/registration-client';
    public const REGISTER_EXPERT = 'registration/registration-expert';
    public const REGISTER_CLINIC = 'registration/registration-clinic';
    public const REGISTER_INSURER = 'registration/registration-insurer';
    public const REGISTER_COMPANY_EXPERT = 'registration/registration-company-expert';
    public const RENEW_LICENSE = 'reminder/renew-licences';
    public const BOOK_SERVICE = 'reminder/booking-services';
    public const PARTICIPATION_IN_CONCILIUM = 'concilium';
    public const BOOK_SERVICE_GENERIC = 'reminder/booking-services-generic';
    public const SERVICE_REMINDER = 'reminder/services-reminder';
    public const NOT_MONITORED = 'reminder/not-monitored';
    public const ANALYSE_WARNING = 'reminder/analyse-warning';
    public const HEALTH_REMIDER = 'reminder/health-dynamic-reminder';
    public const TELEGRAM = 'telegram';
    public const SYNCHRONIZATION_TELEGRAM = 'telegram/telegram-synchronization';
    public const FORGOT_PASSWORD = 'company-expert-reset-password';
    public const BASE_MAILING = 'base_mailing';
    public const WALLET_CLIENT_APPROVE_WITHDRAW = 'wallet/client-approve-withdraw';
    public const WALLET_ADMIN_APPROVE_WITHDRAW = 'wallet/admin-approve-withdraw';
}
