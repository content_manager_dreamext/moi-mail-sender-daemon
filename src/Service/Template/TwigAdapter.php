<?php


namespace App\Service\Template;


use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\Templating\EngineInterface;
use Symfony\Contracts\Translation\TranslatorInterface;
use Twig\Environment;
use Twig\Loader\FilesystemLoader;

class TwigAdapter implements EngineInterface
{
    protected $twig;
    protected $loader;
    protected $translator;

    public function __construct(ParameterBagInterface $parameterBag, TranslatorInterface $translator)
    {
        $this->loader = new FilesystemLoader($parameterBag->get('templates_directory'));
        $this->twig = new Environment($this->loader, [
            'cache' => $parameterBag->get('templates_cache_directory'),
        ]);
        $this->translator = $translator;
        $filter = new \Twig\TwigFilter('trans', [
            $this,
            'translate'
        ]);
        $this->twig->addFilter($filter);
        $this->twig->addGlobal('domain', $parameterBag->get('DOMAIN'));

    }

    public function translate(string $text, array $params = [], string $domain = 'messages', string $locale = 'uk') {
        return $this->translator->trans($text, $params, $domain, $locale);
    }

    /**
     * @inheritDoc
     */
    public function render($name, array $parameters = [])
    {
        return $this->twig->render($name, $parameters);
    }

    /**
     * @inheritDoc
     */
    public function exists($name)
    {
        return $this->loader->exists($name);
    }

    /**
     * @inheritDoc
     */
    public function supports($name)
    {
        return preg_match('/\.twig\./is', $name);
    }
}