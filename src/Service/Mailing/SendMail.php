<?php

namespace App\Service\Mailing;

use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;

class SendMail implements SendMailInterface
{
    protected $mailer;
    protected $parameterBag;

    public function __construct(\Swift_Mailer $mailer, ParameterBagInterface $parameterBag)
    {
        $this->mailer = $mailer;
        $this->parameterBag = $parameterBag;
    }

    public function sendEmail(string $subject, string $email, string $body) {
        $message = new \Swift_Message();
        $message->setSubject($subject)
            ->setFrom(
                [
                    $this->parameterBag->get('APP_EMAIL') => $this->parameterBag->get('APP_EMAIL_TITLE')
                ]
            )
            ->setTo($email)
            ->setBody($body, 'text/html');
        try {
            $this->mailer->send($message);
        } catch (\Exception $e) {
        }
    }
}