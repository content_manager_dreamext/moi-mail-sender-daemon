<?php

namespace App\Service\Mailing;

interface SendMailInterface
{
    public function sendEmail(string $subject, string $email, string $body);
}