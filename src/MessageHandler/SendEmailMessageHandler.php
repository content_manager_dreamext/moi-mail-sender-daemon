<?php

namespace App\MessageHandler;

use App\Message\SendEmailMessage;
use App\Service\Mailing\SendMailInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ContainerBagInterface;
use Symfony\Component\Messenger\Handler\MessageHandlerInterface;
use Symfony\Component\Templating\EngineInterface;

class SendEmailMessageHandler implements MessageHandlerInterface
{
    protected $engine;
    protected $sendMail;
    protected $logger;
    protected $sendEmailAboutError;
    protected $contactEmail;
    protected $sendControlLetter;
    protected $controlEmail;

    public function __construct(
        EngineInterface $engine,
        SendMailInterface $sendMail,
        LoggerInterface $logger,
        ContainerBagInterface $container
    )
    {
        $this->engine = $engine;
        $this->sendMail = $sendMail;
        $this->logger = $logger;
        $this->sendEmailAboutError = $container->get('send_email_about_error');
        $this->contactEmail = $container->get('contact_email');
        $this->sendControlLetter = $container->get('send_control_letter');
        $this->controlEmail = $container->get('control_email');
    }
    public function __invoke(SendEmailMessage $sendEmailMessage)
    {
        $template = $sendEmailMessage->getTemplate() . '.html.twig';
        $body = $this->renderMailBody($template, $sendEmailMessage);
        $body && $this->sendMailMessage(
            $sendEmailMessage,
            $body,
            $template
        );
    }

    protected function renderMailBody(
        string $template,
        SendEmailMessage $sendEmailMessage
    ) {
        $body = '';
        try {
            $body = $this->engine->render($template, [
                'subject' => $sendEmailMessage->getSubject(),
                'data' => $sendEmailMessage->getData(),
                'locale' => $sendEmailMessage->getLocale() ?? "uk"
            ]);
        } catch (\Exception $exception) {
            if ($this->sendEmailAboutError) {
                $this->sendDebugEmail(
                    $sendEmailMessage,
                    'Помилка надсилання пошти',
                    'error.html.twig',
                    $this->contactEmail,
                    [
                        'template' => $template,
                        'locale' => $sendEmailMessage->getLocale() ?? "uk",
                        'subject' => $sendEmailMessage->getSubject(),
                        'user' => $sendEmailMessage->getEmail(),
                        'step' => 'Формування шаблону',
                        'exception' => $exception
                    ]
                );
            }
            $this->logger->error('Email send error', [
                'step' => 'Render template',
                'user' => $sendEmailMessage->getEmail(),
                'subject' => $sendEmailMessage->getSubject(),
                'template' => $template,
                'locale' => $sendEmailMessage->getLocale() ?? "uk",
                'exception' => $exception
            ]);
        }

        return $body;
    }

    /**
     * @param SendEmailMessage $sendEmailMessage
     * @param $body
     * @param $template
     * @return $this
     */
    protected function sendMailMessage(
        SendEmailMessage $sendEmailMessage,
        $body,
        $template
    ) {
        try {
            $this->sendMail->sendEmail(
                $sendEmailMessage->getSubject(),
                $sendEmailMessage->getEmail(),
                $body
            );

            if ($this->sendControlLetter) {
                $this->sendDebugEmail(
                    $sendEmailMessage,
                    'Контрольний лист',
                    'control_letter.html.twig',
                    $this->contactEmail,
                    [
                        'template' => $template,
                        'locale' => $sendEmailMessage->getLocale() ?? "uk",
                        'subject' => $sendEmailMessage->getSubject(),
                        'user' => $sendEmailMessage->getEmail(),
                    ]
                );
            }

            $this->logger->info('Email send success', [
                'subject' => $sendEmailMessage->getSubject(),
                'user' => $sendEmailMessage->getEmail(),
                'template' => $template
            ]);
        } catch (\Exception $exception) {
            if ($this->sendEmailAboutError) {
                $this->sendDebugEmail(
                    $sendEmailMessage,
                    'Помилка надсилання пошти',
                    'error.html.twig',
                    $this->contactEmail,
                    [
                        'template' => $template,
                        'locale' => $sendEmailMessage->getLocale() ?? "uk",
                        'subject' => $sendEmailMessage->getSubject(),
                        'user' => $sendEmailMessage->getEmail(),
                        'step' => 'Відправки пошти',
                        'exception' => $exception
                    ]
                );
            }
            $this->logger->error('Email send error', [
                'step' => 'Send Email',
                'user' => $sendEmailMessage->getEmail(),
                'subject' => $sendEmailMessage->getSubject(),
                'template' => $template,
                'locale' => $sendEmailMessage->getLocale() ?? "uk",
                'exception' => $exception
            ]);
        }
        return $this;
    }

    /**
     * Send debug email method
     *
     * @param string $debugEmailSubject
     * @param SendEmailMessage $sendEmailMessage
     * @param string $debugEmailTemplate
     * @param string $debugEmail
     * @param array $data
     */
    protected function sendDebugEmail(
        SendEmailMessage $sendEmailMessage,
        string $debugEmailSubject,
        string $debugEmailTemplate,
        string $debugEmail,
        array $data
    ) {
        $body = $this->engine->render($debugEmailTemplate, [
            'subject' => $debugEmailSubject,
            'data' => $data,
            'locale' => $sendEmailMessage->getLocale() ?? "uk"
        ]);
        $this->sendMail->sendEmail(
            $debugEmailSubject,
            $debugEmail,
            $body
        );
    }
}